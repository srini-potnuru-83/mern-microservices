const CommentList = ({ comments }) => {
  
  const renderedComments = comments.map((comment) => {
    let content;
    if (comment.status === "rejected")
      content = "This Comment is rejected";
    else if(comment.status === "pending")
      content = "This comment is pending moderation";
    else
      content = comment.content;

    return <li key={comment.id}>{content}</li>;
  });

  return <ul>{renderedComments}</ul>;
};

export default CommentList;